# Rowdy Pix
My first raspberry pi project. When complete RowdyPix will take photos when an abnormal amount of noise occurs in the room.

Requirements:

* Raspberry Pi
* Microphone - I used the PS3EYE because it has a camera too
* Camera Pi Cam works great if you care about quality

## Todo
Add face detection to only take pictures when person(s) are in the room.

Take a photo when more than one person is smiling

A floating threshold to determine when to capture the image.

Clean up code.
