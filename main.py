#import rospy
#from std_msgs.msg import String
#from std_msgs.msg import Int32MultiArray
#sudo apt-get install python-pyaudio
import pyaudio
#from rospy.numpy_msg import numpy_msg
#import numpy as np
import time
#import signal
import os
import sys
import audioop
import time, threading
import numpy as np
from picamera import PiCamera
from PIL import Image


CHUNK = 3200
FORMAT = pyaudio.paInt16
CHANNELS = 4
RATE = 16000
DEV_IDX = 5
p = pyaudio.PyAudio()
TAKE_PIC = 200
MAX_PIX =  100

camera = PiCamera()
camera.resolution = (1024, 768)
camera.start_preview()

## warm up camera
time.sleep(2)

pic_count = 0

current_average = 200
rms_s = []

def save_image():
    global camera
    file_name = time.strftime("%Y-%m-%d_%H-%M-%S")
    file_jpg = 'images/'+file_name + '.jpg'
    camera.capture(file_jpg)
    global pic_count
    pic_count = pic_count + 1
    






def calc():
    global rms_s, current_average
    current_average = np.mean(rms_s)
    print time.ctime()
    print 'average rms ', current_average
    foo()

def foo():
    global pic_count, MAX_PIX
    if pic_count > MAX_PIX:
        return;
    threading.Timer(1, calc).start()

foo()

def get_change(current, previous):
    if current == previous:
        return 0
    try:
       return ((abs(current - previous))/previous)*100.0
    except ZeroDivisionError:
        return 0

def callback(in_data, frame_count, time_info, status):
    global rms_s,current_average, cam
    
    rms = audioop.rms(in_data,2)
    p_change = get_change(rms,current_average)
    #print 'percent change: ',p_change

    if len(rms_s) > 10:
        rms_s.pop()

    rms_s.append(rms)
    #print rms

    if p_change > TAKE_PIC and len(rms_s) > 8:
        print "taking picture %s avg: %s" % (rms, current_average)
        save_image()

    return (in_data, pyaudio.paContinue)


stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK,
                input_device_index=2,
                stream_callback=callback)
stream.start_stream()
live_stream = stream.is_active()

while live_stream:
    time.sleep(0.1)

    if pic_count > MAX_PIX:
        live_stream = False

stream.close()
p.terminate()
